import java.util.Scanner;

public class Stage5 {
    public static void main(String[] args) {
        int machineWater = 400;
        int machineMilk = 540;
        int machineCoffeeBeans = 120;
        int machineCups = 9;
        int machineMoney = 550;

        int water = 0;
        int milk = 0;
        int beans = 0;
        int price = 0;
        int cups = 0;

        Scanner sc = new Scanner(System.in);

        boolean exit = false;
        while (!exit) {
            System.out.println("Write action (buy, fill, take, remaining, exit): ");
            switch (sc.nextLine()) {
                case "remaining":
                    System.out.printf("%d ml of water \n%d ml of milk \n%d g of coffee beans \n%d disposable cups \n$%d of money\n",
                        machineWater,
                        machineMilk,
                        machineCoffeeBeans,
                        machineCups,
                        machineMoney);
                break;
            
                case "fill":
                    System.out.println("Write how many ml of water you want to add: ");
                    machineWater += sc.nextInt();
                    System.out.println("Write how many ml of milk you want to add: ");
                    machineMilk += sc.nextInt();
                    System.out.println("Write how many grams of coffee beans you want to add: ");
                    machineCoffeeBeans += sc.nextInt();
                    System.out.println("Write how many disposable cups of coffee you want to add: ");
                    machineCups += sc.nextInt();
                break;

                case "take":
                    System.out.println("I gave you $" + machineMoney);
                    machineMoney = 0;
                break;

                case "exit": 
                    exit = true;
                break;

                case "buy": 
                    System.out.println("What do you want to buy? 1 - espresso, 2 - latte, 3 - cappuccino, back - to main menu:");
                    String option = sc.nextLine();
                    if(option.compareTo("back") == 0) {
                        continue;
                    }
                    int cafeType = Integer.parseInt(option);
                    if (cafeType == 1) {
                        water = 250;
                        beans = 16;
                        price = 4;
                        cups = 1;
                    } else if (cafeType == 2) {
                        water = 350;
                        milk = 75;
                        beans = 20;
                        price = 7;
                        cups = 1;
                    } else if (cafeType == 3) {
                            water = 200;
                            milk = 100;
                            beans = 12;
                            price = 6;
                            cups = 1;
                    }

                    boolean makeable = (water <= machineWater) && (milk <= machineMilk) && (beans <= machineCoffeeBeans) && (cups <= machineCups);

                    if (makeable) {
                        machineWater -= water;
                        machineMilk -= milk;
                        machineCoffeeBeans -= beans;
                        machineCups -= cups;
                        machineMoney += price;
                        System.out.println("I have enough resources, making you a coffee!");
                    } else {
                        String shortage = "";
                        if (water > machineWater) {
                            shortage += " water";
                        }
                        if (milk > machineMilk) {
                            shortage += " milk";
                        }
                        if (beans > machineCoffeeBeans) {
                            shortage += " coffee beans";
                        }
                        if (cups > machineCups) {
                            shortage += " disposable cups";
                        }
                        System.out.println("Sorry, not enough" + shortage + "!");
                    }
                break;
            }
        }
        sc.close();
    }
}
