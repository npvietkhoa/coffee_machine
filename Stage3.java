import java.util.Scanner;
public class Stage3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Write how many ml of water the coffee machine has: ");
        int machineWater = sc.nextInt();

        System.out.println("Write how many ml of milk the coffee machine has: ");
        int machineMilk = sc.nextInt();

        System.out.println("Write how many grams of coffee beans the coffee machine has: ");
        int machineCoffeBean = sc.nextInt();

        System.out.println("Write how many cups of coffee you will need: ");
        int cups = sc.nextInt();

        sc.close();

        int maxCups = Math.min(machineWater / 200, Math.min(machineCoffeBean / 15, machineMilk / 50));

        if (cups == maxCups) {
            System.out.printf("Yes, I can make that amount of coffee");
        } else if (cups < maxCups) {
            System.out.printf("Yes, I can make that amount of coffe (and even %d more than that)", maxCups - cups);
        } else {
            System.out.printf("No, I can make only %d cup(s) of coffee", maxCups);
        }
    }
}
