import java.util.Scanner;
public class Stage4 {
    public static void main(String[] args) {
        int machineWater = 400;
        int machineMilk = 540;
        int machineCoffeeBeans = 120;
        int machineCups = 9;
        int machineMoney = 550;

        System.out.printf("%d ml of water \n%d ml of milk \n%d g of coffee beans \n%d disposable cups \n$%d of money",
                            machineWater,
                            machineMilk,
                            machineCoffeeBeans,
                            machineCups,
                            machineMoney);
                        
        Scanner sc = new Scanner(System.in);
        System.out.println("Write action (buy, fill, take):");
        String action = sc.nextLine();

        int water = 0;
        int milk = 0;
        int beans = 0;
        int price = 0;
        int cups = 0;

        switch (action) {
            case "buy":
                System.out.println("What do you want to buy? 1 - espresso, 2 - latte, 3 - cappuccino: ");
                int coffeType = sc.nextInt();

                    switch (coffeType) {
                        case 1:
                            water = 250;
                            beans = 16;
                            price = 4;
                            cups = 1;
                            break;
                        case 2:
                            water = 350;
                            milk = 75;
                            beans = 20;
                            price = 7;
                            cups = 1;
                            break;
                        default:
                            water = 200;
                            milk = 100;
                            beans = 12;
                            price = 6;
                            cups = 1;
                            break;
                    }
                break;
            case "take":
                System.out.println("I gave you $" + machineMoney);
                price = -machineMoney;
                break;
            case "fill":
                System.out.println("Write how many ml of water you want to add: ");
                machineWater += sc.nextInt();
                System.out.println("Write how many ml of milk you want to add: ");
                machineMilk += sc.nextInt();
                System.out.println("Write how many grams of coffee beans you want to add: ");
                machineCoffeeBeans += sc.nextInt();
                System.out.println("Write how many disposable cups of coffee you want to add: ");
                machineCups += sc.nextInt();
                break;
        }
        sc.close();
        System.out.println("The coffee machine has: ");
        System.out.printf("%d ml of water \n%d ml of milk \n%d g of coffee beans \n%d disposable cups \n$%d of money",
                            machineWater - water,
                            machineMilk - milk,
                            machineCoffeeBeans - beans,
                            machineCups - cups,
                            machineMoney + price);
    }
}
