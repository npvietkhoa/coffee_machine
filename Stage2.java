package machine;
import java.util.Scanner;
public class Stage2 {
    public static void main(String[] args) {
        System.out.println("Write how many cups of coffe you will need:");
        Scanner sc = new Scanner(System.in);
        int cups = sc.nextInt();
        sc.close();
        System.out.printf("For %d cups of coffe you will need:\n", cups);
        System.out.printf("%d ml of water\n%d ml of milk \n%d g of coffe beans", cups * 200, cups * 50, cups * 15); 
    }
}
