import java.util.Scanner;

public class Stage6 {
    
    static int machine_water = 400;
    static int machine_milk = 540;
    static int machine_coffee = 120;
    static int machine_cups = 9;
    static int machine_money = 550;

    enum Action {
        BUY,
        FILL,
        TAKE,
        REMAINING,
        EXIT
    }

    static void take() {
        System.out.println("I gave you $" + machine_money);
        machine_money = 0;
    }

    static void remaining() {
        System.out.println("The coffee machine has:");
        System.out.println(machine_water + " ml of water");
        System.out.println(machine_milk + " ml of milk");
        System.out.println(machine_coffee + " g of coffee beans");
        System.out.println(machine_cups + " disposable cups");
        System.out.printf("$%d of money\n", machine_money); 
    }

    static void fill() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Write how many ml of water you want to add: ");
        machine_water += sc.nextInt();
        System.out.println("Write how many ml of milk you want to add: ");
        machine_milk += sc.nextInt();
        System.out.println("Write how many grams of coffee beans you want to add: ");
        machine_coffee += sc.nextInt();
        System.out.println("Write how many disposable cups of coffee you want to add: ");
        machine_cups += sc.nextInt();
    }

    static boolean check_ingredient(int ing, int machine_ing) {
        return machine_ing >= ing;
    }
    static void brew_coffee(int water, int milk, int coffee, int money) {
        if(!check_ingredient(water, machine_water)) {
            System.out.println("Sorry, not enough water!");
            return;
        } else if(!check_ingredient(milk, machine_milk)) {
            System.out.println("Sorry, not enough milk!");
            return;
        } else if (!check_ingredient(coffee, machine_coffee)) {
            System.out.println("Sorry, not enough coffee beans!");
            return;
        } else if (machine_cups <= 0) {
            System.out.println("Sorry, not enough cups!");
            return;
        }

        System.out.println("I have enough resources, making you a coffee!");
        machine_water -= water;
        machine_coffee -= coffee;
        machine_cups -= 1;
        machine_milk -= milk;
        machine_money += money;
    }

    static void buy() {
        System.out.println("What do you want to buy? 1 - espresso, 2 - latte, 3 - cappuccino, back - to main menu: ");
        Scanner sc = new Scanner(System.in);
        String option = sc.nextLine();
        if (option.equals("back")) {
            return;
        }
        int option_number = Integer.parseInt(option);
        switch (option_number) {
            case 1:
                brew_coffee(250, 0, 16, 4);
                break;
            case 2:
                brew_coffee(350, 75, 20, 7);
                break;
            case 3:
                brew_coffee(200, 100, 12, 6);
                break;
            default:
                break;
        }
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Action user_action = null;
        while(!Action.EXIT.equals(user_action)) {
            System.out.println("Write action (buy, fill, take, remaining, exit): ");
            user_action = Action.valueOf(sc.nextLine().toUpperCase());
            switch (user_action) {
                case BUY:
                    buy();
                    break;
                case FILL:
                    fill();
                    break;
                case TAKE:
                    take();
                    break;
                case REMAINING:
                    remaining();
                    break;
                default:
                    break;
            }
        }
        sc.close();
    }
}
